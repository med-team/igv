igv (2.18.5+dfsg-1) unstable; urgency=medium

  * New upstream version 2.18.5+dfsg
  * Refreshing patches
  * Raising Standards version to 4.7.0 (no change)
  * Refreshing the list of files to exclude when repacking
  * Skipping more tests requiring the network

 -- Pierre Gruet <pgt@debian.org>  Sun, 13 Oct 2024 21:58:58 +0200

igv (2.17.3+dfsg-1) unstable; urgency=medium

  [ Vladimir Petko ]
  * d/p/skip_tests_network.patch: add tests requiring internet
    (Closes: #1065646).

  [ Andreas Tille ]
  * New upstream version
  * d/watch: Fix download file name

 -- Andreas Tille <tille@debian.org>  Fri, 08 Mar 2024 18:55:28 +0100

igv (2.17.1+dfsg-1) unstable; urgency=medium

  * New upstream version 2.17.1+dfsg
  * Refreshing patches
  * Build-Depending on junit5 for the vintage part of the tests
  * Updating Maven rules
  * Refreshing d/copyright
  * Raising source/target level for the build
  * Avoiding sealed interfaces, which cause errors in the build
  * Dropping tests using unpackaged spark
  * Omitting tests using removed blat data
  * Updating Lintian override for file with very long lines

 -- Pierre Gruet <pgt@debian.org>  Sat, 17 Feb 2024 19:30:09 +0100

igv (2.16.2+dfsg-2) unstable; urgency=medium

  * Solving ambiguity to build against htsjdk/4.0.2+dfsg (Closes: #1058683)

 -- Pierre Gruet <pgt@debian.org>  Thu, 14 Dec 2023 14:09:05 +0100

igv (2.16.2+dfsg-1) unstable; urgency=medium

  * New upstream version 2.16.2+dfsg
  * Fixing the clean target (Closes: #1048443)

 -- Pierre Gruet <pgt@debian.org>  Sun, 08 Oct 2023 13:57:54 +0200

igv (2.16.1+dfsg-1) unstable; urgency=medium

  * New upstream version 2.16.1+dfsg
  * Refreshing patches
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Pierre Gruet <pgt@debian.org>  Mon, 26 Jun 2023 12:09:14 +0200

igv (2.16.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.16.0+dfsg
  * Refreshing patches

 -- Pierre Gruet <pgt@debian.org>  Wed, 01 Feb 2023 22:47:28 +0100

igv (2.15.4+dfsg-1) unstable; urgency=medium

  * New upstream version 2.15.4+dfsg
  * Refreshing patches
  * Raising Standards version to 4.6.2 (no change)

 -- Pierre Gruet <pgt@debian.org>  Fri, 06 Jan 2023 22:39:09 +0100

igv (2.15.2+dfsg-1) unstable; urgency=medium

  * New upstream version 2.15.2+dfsg
  * Refreshing patches
  * Using the static form of URLDecoder.decode to build against OpenJDK17

 -- Pierre Gruet <pgt@debian.org>  Wed, 16 Nov 2022 22:52:56 +0100

igv (2.14.0+dfsg-2) unstable; urgency=medium

  * Excluding one more test requiring the network, thanks to Gianfranco
    Costamagna (Closes: #1018751)

 -- Pierre Gruet <pgt@debian.org>  Wed, 31 Aug 2022 12:35:12 +0200

igv (2.14.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.14.0+dfsg
  * Refreshing patches
  * Excluding more tests requiring the network, thanks to Gianfranco Costamagna
    (Closes: #1018683)
  * Reworking build dependencies, honouring B-D-Indep
  * Removing a wrong Override annotation by patching a class file
  * Adding a Lintian override for very-long-lines in a file

 -- Pierre Gruet <pgt@debian.org>  Mon, 29 Aug 2022 17:15:30 +0200

igv (2.13.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.13.0+dfsg
  * Refreshing patches
  * Setting Standards version to 4.6.1 (no change)
  * Avoiding the instantiation of URLDecoder as we only call a static method
    (Closes: #1012071)

 -- Pierre Gruet <pgt@debian.org>  Tue, 31 May 2022 22:09:01 +0200

igv (2.12.3+dfsg-1) unstable; urgency=medium

  * New upstream version 2.12.3+dfsg
  * Refreshing patches
  * Removing obsolete dependency on goby
  * Remove trailing whitespace in debian/copyright (routine-update)

 -- Pierre Gruet <pgt@debian.org>  Fri, 06 May 2022 22:09:11 +0200

igv (2.12.0+dfsg-1) unstable; urgency=medium

  * New upstream version 2.12.0+dfsg
  * Refreshing patches
  * Polishing d/copyright
  * Skipping more tests needing the network

 -- Pierre Gruet <pgt@debian.org>  Wed, 09 Feb 2022 17:13:59 +0100

igv (2.11.9+dfsg-1) unstable; urgency=medium

  * New upstream version 2.11.9+dfsg
  * Refreshing d/copyright
  * Refreshing patches
  * Cleaning d/rules
  * Adapting a property name to old Debian-packaged gradle
  * Adding Maven coordinates of jide in build.gradle
  * Building without the AWS classes, as the AWS SDK is not packaged yet
  * Adding a debian/README.Debian file to tell about those missing classes
  * Avoiding running Spark-related things in the tests
  * Providing additional Maven rules and ignoreRules
  * Refreshing dependencies
  * Refreshing the classpath of the built jar
  * Excluding more tests needing the network
  * Removing old debian/README.source file
  * Excluding HTML files with very long lines from the source tarball and
    deactivating (previously passing) tests that read them

 -- Pierre Gruet <pgt@debian.org>  Thu, 23 Dec 2021 16:47:23 +0100

igv (2.6.3+dfsg2-2) unstable; urgency=medium

  * Source-only upload
  * Adding a metainfo file for AppStream

  [ Andreas Tille ]
  * Fix watch file

 -- Pierre Gruet <pgt@debian.org>  Fri, 12 Nov 2021 17:34:33 +0100

igv (2.6.3+dfsg2-1) unstable; urgency=medium

  * New upstream version 2.6.3+dfsg2
  * Switching from non-free to main:
    - Removing all embedded jar files
    - Removing the non-free iconset, adding home-made ones
  * Refreshing patches
  * Raising Standards version to 4.6.0 (no change)
  * Updating d/watch for Github new release/tag page
  * Refreshing d/copyright
  * Depending on the Debian-packaged libgoby-io-java
  * Adding jars in the classpath for the tests
  * Indicating the coordinates of Debian-packaged dependencies and omitting
    modules
  * Omitting three tests related to goby as they rely on too old data files
  * Providing an icon and a desktop entry

 -- Pierre Gruet <pgt@debian.org>  Sun, 07 Nov 2021 13:54:10 +0100

igv (2.6.3+dfsg-3) unstable; urgency=medium

  * Revising the list of dependencies and updating d/igv.manifest accordingly
  * Refreshing d/copyright
  * Lightening d/bin/igv as igv.jar has a sufficient classpath

 -- Pierre Gruet <pgtdebian@free.fr>  Fri, 13 Nov 2020 20:21:39 +0100

igv (2.6.3+dfsg-2) unstable; urgency=medium

  * Correcting the module name of junit in build.gradle
    Closes: #974076

 -- Pierre Gruet <pgtdebian@free.fr>  Wed, 11 Nov 2020 07:12:39 +0100

igv (2.6.3+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Standards-Version: 4.5.0 (Rules-Requires-Root field added)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Pierre Gruet ]
  * New upstream version
    Closes: #940606
  * Updating build directory to Java 11
  * Add runtime dependency to batik
  * Adapting paths for gradle as we use Debian libs
  * Adapting the code, written for an old htsjdk version
    Closes: #968739
  * Skipping build-time tests that require an access to network

 -- Pierre Gruet <pgtdebian@free.fr>  Tue, 01 Sep 2020 22:14:41 +0200

igv (2.4.17+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Wed, 30 Jan 2019 14:45:04 +0100

igv (2.4.16+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Drop Build-Depends: libmysql-java, Recommends: libmariadb-java
    Closes: #913323

 -- Andreas Tille <tille@debian.org>  Thu, 06 Dec 2018 09:11:58 +0100

igv (2.4.14+dfsg-2) unstable; urgency=medium

  [ Olivier Sallou ]
  fix for FTBS openjdk 11, Closes: #895765
    - thanks to Markus Koschany <apo@debian.org>

 -- Olivier Sallou <osallou@debian.org>  Tue, 23 Oct 2018 14:55:18 +0200

igv (2.4.14+dfsg-1) unstable; urgency=medium

  [ Dylan Aïssi ]
  * Add more libraries deps in classpath
      (LP: #1400648, #1476650, #1602389)

  [ Andreas Tille ]
  * New upstream version
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Upstream switched build system to gradle
  * Add commons-io.jar and gson.jar to classpath in wrapper script
    (Thanks for the hint to Tony Travis <tony.travis@minke-informatics.co.uk>)
    (LP: #1797819)
  * XS-Autobuild: yes
  * d/copyright: Maintain Files-Excluded:
     - jlfgr is now available as source code not binary jar
     - do not mention files not in tarball any more
     - remove more unused jars
  * Add missing Depends

  [ Olivier Sallou]
  * d/patches/fix_gradle.patch: adapt build to Debian
    Remove java module support as some Debian libs are not modularized yet

 -- Andreas Tille <tille@debian.org>  Fri, 19 Oct 2018 10:52:29 +0200

igv (2.4.6+dfsg-1) unstable; urgency=medium

  [ Steffen Moeller ]
  * debian/upstream/metadata:
    - Added references to registries
    - yamllint cleanliness

  [ Andreas Tille ]
  * New upstream version
  * build.xml: `include name="batik.jar"` (Thanks for the hint to Markus
    Koschany <apo@debian.org>)
    Closes: #877941
  * Standards-Version: 4.1.3
  * New Build-Depends: openjfx
  * Drop libconcurrent-java from Build-Depends
  * debhelper 11

 -- Andreas Tille <tille@debian.org>  Mon, 22 Jan 2018 09:52:36 +0100

igv (2.3.90+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream version.

 -- Sascha Steinbiss <satta@debian.org>  Sat, 14 Jan 2017 08:41:54 +0000

igv (2.3.88+dfsg-1) unstable; urgency=medium

  * New upstream version
    Closes: #846474
  * d/watch:
     - version=4
     - repack + xz compression
  * Replace code copies of libmysql-java, gson and libfest
  * cme fix dpkg-control
  * debhelper 10

 -- Andreas Tille <tille@debian.org>  Tue, 13 Dec 2016 12:18:16 +0100

igv (2.3.81+dfsg-1) unstable; urgency=medium

  * New upstream release 2.3.81+dfsg

 -- Olivier Sallou <osallou@debian.org>  Tue, 27 Sep 2016 11:36:47 +0200

igv (2.3.68+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Add Build-Depends: libhtsjdk-java (and leave libsam-java for
    potential backports)
    Closes: #810842

 -- Andreas Tille <tille@debian.org>  Thu, 14 Jan 2016 16:39:12 +0100

igv (2.3.64+dfsg-1) unstable; urgency=medium

  * New upstream version
    Closes: #804393
  * License change to MIT

 -- Andreas Tille <tille@debian.org>  Sun, 08 Nov 2015 08:22:41 +0100

igv (2.3.57+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Removed Shaun Jackman from uploaders

 -- Andreas Tille <tille@debian.org>  Fri, 17 Jul 2015 21:59:26 +0200

igv (2.3.38+dfsg-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Fri, 24 Oct 2014 23:13:16 +0200

igv (2.3.36+dfsg-1) unstable; urgency=medium

  * New upstream version (refreshed patches)
  * Adapt watch file to new location (github)
  * Adapt to new uscan
  * New Build-Depends: libcommons-io-java
  * Drop Build-Depends libnb-absolutelayout-java and rather leave the
    binary lib/AbsoluteLayout.jar in since we have JARs there anyway
  * d/copyright: Updated regarding changed file names

 -- Andreas Tille <tille@debian.org>  Fri, 15 Aug 2014 11:16:32 +0200

igv (2.3.14+dfsg-2) unstable; urgency=medium

  * debian/upstream -> debian/upstream/metadata
  * (Build-)Depends: libcofoja-java
    Closes: #741052
  * cme fix dpkg-control
  * Add missing default-jdk in Build-Depends and use ${java:Depends}
    instead of repeating dependencies manually

 -- Andreas Tille <tille@debian.org>  Mon, 17 Mar 2014 20:22:24 +0100

igv (2.3.14+dfsg-1) unstable; urgency=low

  * New upstream version
  * debian/copyright: Removed Files: lib/jide* section because it is
    not shipped any more in source in favour of the Debian packaged
    version
    Closes: #722677
  * debian/control:
     - cme fix dpkg-control
     - debhelper 9

 -- Andreas Tille <tille@debian.org>  Fri, 13 Sep 2013 14:03:08 +0200

igv (2.3.12+dfsg-1) unstable; urgency=low

  * New upstream release

 -- Olivier Sallou <osallou@debian.org>  Thu, 25 Jul 2013 16:32:45 +0200

igv (2.3.2+dfsg-2) unstable; urgency=low

  * Move to unstable.

 -- Olivier Sallou <osallou@debian.org>  Sun, 02 Jun 2013 11:05:35 +0200

igv (2.3.2+dfsg-1) experimental; urgency=low

  * New upstream release
  * Use libjide-oss-java from Debian (Closes: #703721).
  * Use libcofoja-java from Debian

 -- Olivier Sallou <osallou@debian.org>  Wed, 01 May 2013 12:28:03 +0200

igv (2.2.7+dfsg-1) experimental; urgency=low

  [ Andreas Tille ]
  * New upstream version
  * debian/upstream: Add citation information
  * debian/control:
     - Enhanced long description
     - Standards-Version: 3.9.3 (no changes needed)
     - Enable JDK transition (thanks to James Page <james.page@ubuntu.com>
       for the patch)
       Closes: #683538
  * debian/copyright:
     - Proper DEP5 Format field
     - Fixed Source field
     - Added Files-Excluded to document what files were excluded from upstream
  * debian/rules: get-orig-source using uscan from Git to exclude
    Files-excluded automatically

 -- Olivier Sallou <osallou@debian.org>  Sat, 23 Feb 2013 22:24:32 +0100

igv (2.0.30-1) unstable; urgency=low

  * New upstream release.

 -- Shaun Jackman <sjackman@debian.org>  Thu, 22 Dec 2011 16:59:30 -0800

igv (2.0.6-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Depend and Build-Depend on libcommons-logging-java,
    libhttpclient-java, libhttpcore-java, libjcommon-java and
    libjfreechart-java.
  * debian/igv.1: Update.
  * debian/watch: Update.

 -- Shaun Jackman <sjackman@debian.org>  Mon, 25 Jul 2011 11:41:18 -0700

igv (1.5.64-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * debian/control:
    - Depend and Build-Depend on libjhdf5-java.
    - Incremented Standards-Version to reflect conformance with Policy 3.9.2.
    - Corrected Vcs-Git URL.

 -- Charles Plessy <plessy@debian.org>  Thu, 23 Jun 2011 19:57:52 +0900

igv (1.5.60-1) unstable; urgency=low

  * New upstream release.
  * Remove libjhdf5-java from the dependencies.

 -- Shaun Jackman <sjackman@debian.org>  Mon, 21 Mar 2011 16:34:04 -0700

igv (1.5.21-1) unstable; urgency=low

  * New upstream release.

 -- Shaun Jackman <sjackman@debian.org>  Fri, 17 Sep 2010 17:18:33 -0700

igv (1.5.18-1) unstable; urgency=low

  * New upstream release.

 -- Shaun Jackman <sjackman@debian.org>  Wed, 18 Aug 2010 09:55:29 -0700

igv (1.5.16-1) experimental; urgency=low

  * New upstream release.

 -- Shaun Jackman <sjackman@debian.org>  Fri, 06 Aug 2010 10:22:31 -0700

igv (1.5.14-1) unstable; urgency=low

  * New upstream release.
  * Remove libcolt-java from the dependencies.
  * Bump Standards-Version to 3.9.1.

 -- Shaun Jackman <sjackman@debian.org>  Wed, 28 Jul 2010 16:22:20 -0700

igv (1.5.10-1) unstable; urgency=low

  * New upstream release.

 -- Shaun Jackman <sjackman@debian.org>  Fri, 23 Jul 2010 14:19:24 -0700

igv (1.5.09-1) unstable; urgency=low

  * New upstream release.

 -- Shaun Jackman <sjackman@debian.org>  Tue, 20 Jul 2010 09:41:10 -0700

igv (1.5.07-1) unstable; urgency=low

  * New upstream release.

 -- Shaun Jackman <sjackman@debian.org>  Thu, 15 Jul 2010 09:59:01 -0700

igv (1.5.06-1) UNRELEASED; urgency=low

  * New upstream release.
  * Depend on libcolt-java.

 -- Shaun Jackman <sjackman@debian.org>  Wed, 14 Jul 2010 11:31:22 -0700

igv (1.5.05-1) UNRELEASED; urgency=low

  * New upstream release.
  * Added missing build-dependency: libjlibeps-java (debian/control).
  * Removed the fourth number in Standards-Version (debian/control,
    see Policy §5.6.11).
  * Refreshed, renamed and added DEP-3 headers to the patch to the
    upstream build system (debian/patches/01-build.xml)

 -- Charles Plessy <plessy@debian.org>  Thu, 08 Jul 2010 13:24:10 +0900

igv (1.5.03-1) unstable; urgency=low

  * New upstream release.

 -- Shaun Jackman <sjackman@debian.org>  Tue, 06 Jul 2010 13:42:43 -0700

igv (1.5.01-1) UNRELEASED; urgency=low

  * New upstream release.

 -- Shaun Jackman <sjackman@debian.org>  Fri, 02 Jul 2010 10:26:19 -0700

igv (1.4.2-1) UNRELEASED; urgency=low

  * Initial release. Closes: #585457.

 -- Shaun Jackman <sjackman@debian.org>  Sun, 06 Jun 2010 12:41:59 -0700
